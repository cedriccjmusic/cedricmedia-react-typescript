
export interface ITestimonial {
	comment: string;
	author: string;
	id: string;
	img: string;
    altText: string;
    isActive: boolean;
}

export interface INavLink {
    href: string;
    class: string;
    name: string;
    icon?: string;
}

export interface IPortfolio {
    hideClass: string;
    link: string;
    heading: string;
    description: string;
    imgUrl: string;
    altText: string;
}

export interface ISiteConfiguration {
    navLinks: INavLink[];
    portfolio: IPortfolio[];
    testimonials: ITestimonial[];
    services: IService[];
}

export interface IService {
    classes: string;
    heading: string;
    price: number;
    frequency: string;
    services: string[];
}

export interface IContactForm {
    name: string;
	email: string;
	subject: string;
	message: string;
}