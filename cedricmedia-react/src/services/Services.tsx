import React, { Component } from "react";
import "./Services.scss";
import * as models from "../models/cm-models";
import services from "../assets/json/services.json";
import * as siteData from "../firestore/siteDataService";
import { firestore } from "firebase/app";

interface IServiceProps {}
interface IServiceState {
  shouldRenderData: boolean;
}
class Services extends Component<IServiceProps, IServiceState> {
  private services: models.IService[] = [];

  state: IServiceState;

  constructor(props: IServiceProps) {
    super(props);

    this.state = {
      shouldRenderData: false
    };
  }

  componentDidMount() {
    this.getAllData();
  }

  getAllData = () => {
    siteData
      .getSiteData()
      .then(doc => {
        if (doc.exists) {
          let data:
            | firestore.DocumentData
            | undefined = doc.data() as models.ISiteConfiguration;
          // this.setState({ data: data });
          this.services = data.services;

          console.debug("all set: ", this.services);

          this.setState({
            shouldRenderData: true
          });
        } else {
          // doc.data() will be undefined in this case
          // this.setState({ data: null });
          let data = null;
          console.debug("No such document!");
        }
      })
      .catch(error => {
        // this.setState({ data: null });
        let data = null;
        console.debug("Error getting document:", error);
        return data;
      });
  };

  serviceData = () =>
    this.services.map((service, key) => (
      <div className="tm-pricing-table-container" key={key}>
        <div className="tm-pricing-table tm-dark-bg">
          <h2 className="tm-pricing-header tm-orange-text text-center text-uppercase">
            {service.heading}
          </h2>
          <div
            className={
              "tm-pricing-details text-center tm-light-gray-text " +
              service.classes
            }
          >
            <div className="price-tag">
              <div className="price-tag-inner">
                <span className="dollar">$</span>
                <span className="price">{service.price}</span>
                <span className="month text-uppercase">
                  /{service.frequency}
                </span>
              </div>
            </div>
            <ul className="text-center tm-light-gray-text">
              {service.services.map((service, key) => (
                <li key={key}>{service}</li>
              ))}
            </ul>
          </div>
        </div>
        <a href="#">
          <div className="canvas-wrap">
            <div className="triangle-bottom">
              <div className="overlay">
                <p className="tm-dark-text text-uppercase">Sign up</p>
              </div>
            </div>
          </div>
        </a>
      </div>
    ));

  render() {
    return (
      <section
        className="cedricmedia-services tm-section tm-orange-bg-transparent services"
        id="services"
      >
        <img
          src={require("../assets/img/tm-border-white-top.png")}
          alt="Border"
          className="tm-border flipped-image-h"
        ></img>
        <div className="container text-center">{this.serviceData()}</div>
        <img
          src={require("../assets/img/tm-border-black-bottom.png")}
          alt="Border"
          className="tm-border"
        ></img>
      </section>
    );
  }
}

export default Services;
