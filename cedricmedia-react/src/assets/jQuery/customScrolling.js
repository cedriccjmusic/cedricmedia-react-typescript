import $ from "jquery";

$(function () {
	windowNavBarCheck();

	$(window).scroll(function () {
		windowNavBarCheck();
	});

});

export const windowNavBarCheck = () => {
	let shouldShowDarkLogo = false;
	if ($(window).scrollTop() > 92) {
		$(".nav-container").addClass("fixed-bg");
		shouldShowDarkLogo = true;
		return true;
	} else {
		$(".nav-container").removeClass("fixed-bg");
		shouldShowDarkLogo = false;
		return false;
	}
}

