import React, { Component } from "react";
import "./CedricmediaLogo.scss";

interface ILogoProps {}
interface ILogoState {
	logoFadeIn: string;
	logoJack: string;
	letterFadeIn: string;
}

class CedricmediaLogo extends Component<ILogoProps, ILogoState> {
	constructor(props: ILogoProps) {
		super(props);

		this.state = {
			logoFadeIn: "",
			logoJack: "",
			letterFadeIn: ""
		};

		setTimeout(() => {
			this.loadLogo();
		}, 1000);
	}

	loadLogo() {
		setTimeout(() => {
			this.setState({
				logoFadeIn: "fadeIn",
				logoJack: "jackInTheBox animated"
			});
		}, 300);
		setTimeout(() => {
			this.setState({
				letterFadeIn: "fadeIn animated"
			});
		}, 1500);
	}

	render() {
		return (
			<div className="cm-logo">
				<svg
					id="Layer_1"
					className={"cedricmedia-logo " + this.state.logoFadeIn}
					data-name="Layer 1"
					xmlns="http://www.w3.org/2000/svg"
					viewBox="0 0 443.64 350.45"
				>
					<title>cm-2015</title>
					<path
						id="logo_m"
						data-name="logo m"
						className={"cls-1 c-name-letter " + this.state.logoJack}
						d="M368.28,20l-45,77.52L278.33,20c-46,17.77-78.55,62-78.55,113.67,0,47.24,27.17,88.2,66.9,108.49l25.21-43.45A72.18,72.18,0,0,1,267,88.49l56.32,95.8,56.32-95.8a72.18,72.18,0,0,1-24.91,110.18l25.21,43.45c39.74-20.29,66.9-61.25,66.9-108.49C446.82,81.91,414.25,37.73,368.28,20Z"
						transform="translate(-3.18 -9)"
					/>
					<path
						className={"c-name-letter " + this.state.logoJack}
						id="logo_c"
						data-name="logo c"
						d="M184.26,174.61a72.2,72.2,0,1,1,1.3-85.38A122,122,0,0,1,215,45.72,123.88,123.88,0,0,0,126.7,9C58.48,9,3.18,63.64,3.18,131s55.3,122,123.52,122A123.86,123.86,0,0,0,212.35,219,121.85,121.85,0,0,1,184.26,174.61Z"
						transform="translate(-3.18 -9)"
					/>
					<text
						className={"cls-2 bottom-row " + this.state.letterFadeIn}
						transform="translate(0.07 333.15)"
					>
						<tspan className="cls-3">c</tspan>
						<tspan className="cls-4" x="42.23" y="0">
							edric
						</tspan>
						<tspan className="cls-5" x="227.34" y="0">
							media
						</tspan>
					</text>
				</svg>
			</div>
		);
	}
}

export default CedricmediaLogo;
