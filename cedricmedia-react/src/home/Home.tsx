import React, { Component, ReactHTMLElement } from "react";
import "./Home.scss";
import DesktopNav from "../navigation/desktop-nav/DesktopNav";
import MobileNav from "../navigation/mobile-nav/MobileNav";
import CedricmediaLogo from "../modules/logo/CedricmediaLogo";
import Hamburger from "../navigation/hamburger/hamburger";
import * as models from "../models/cm-models";
import * as siteData from "../firestore/siteDataService";
import { firestore } from "firebase/app";
import _ from "lodash";

import $ from "jquery";
import "../assets/jQuery/customScrolling";

interface IHomeProps {}
interface IHomeState {
  isMenuOpen: boolean;
  activeNavLink: number;
  shouldRenderData: boolean;
}

class Home extends Component<IHomeProps, IHomeState> {
  private navLinks: models.INavLink[] = [];

  state: IHomeState;

  constructor(props: IHomeProps) {
    super(props);

    this.state = {
      isMenuOpen: false,
      activeNavLink: 0,
      shouldRenderData: false
    };

    this.scrollActive();
  }
  componentDidMount() {
    this.getAllData();
  }

  getAllData = () => {
    siteData
      .getSiteData()
      .then(doc => {
        if (doc.exists) {
          let data:
            | firestore.DocumentData
            | undefined = doc.data() as models.ISiteConfiguration;
          // this.setState({ data: data });
          this.navLinks = data.navLinks;

          this.setState({
            shouldRenderData: true
          });
        } else {
          // doc.data() will be undefined in this case
          // this.setState({ data: null });
          let data = null;
          console.debug("No such document!");
        }
      })
      .catch(error => {
        // this.setState({ data: null });
        let data = null;
        console.debug("Error getting document:", error);
        return data;
      });
  };

  logoClickHandler = () => {
    this.setState({
      activeNavLink: 0
    });
    $("html, body").animate(
      {
        scrollTop: 0
      },
      "slow"
    );
  };
  hamClickHandler = () => {
    this.setState(prevState => ({
      isMenuOpen: !prevState.isMenuOpen
    }));
  };

  handleNavLinkClick = (key: number) => {
    this.setState({
      activeNavLink: key
    });

    let matchingSection = this.navLinks[key].href;
    let navbarHeight = $(".nav-container").innerHeight();
    console.debug("navbarHeight: ", navbarHeight);
    $("html,body").animate(
      {
        scrollTop: $("" + matchingSection).offset()!.top - navbarHeight!
      },
      "slow"
    );
  };

  scrollActive = () => {
    $(window).scroll(() => {
      let scrollPos = $(document).scrollTop()!;
      let tmBorder = $(".tm-border").innerHeight()!;
      $(".nav-btn").each((index: number, navLink: HTMLElement) => {
        let refElement = $(navLink).attr("href")!;
        if (
          $(refElement).position().top - tmBorder <= scrollPos &&
          $(refElement).position().top + $(refElement).height()! - tmBorder >
            scrollPos
        ) {
          let activeIndex = _.findIndex(this.navLinks, ["href", refElement]);
          this.setState({
            activeNavLink: activeIndex
          });
        }
      });
    });
  };

  navLinkData = () =>
    this.navLinks.map((navLink, key) => (
      <li key={key}>
        <a
          href={navLink.href}
          className={
            "nav-btn " +
            navLink.class +
            (this.state.activeNavLink === key ? " active" : "")
          }
          onClick={() => this.handleNavLinkClick(key)}
        >
          {navLink.name}
        </a>
      </li>
    ));

  render() {
    return (
      <section
        className="cedricmedia-header tm-orange-bg-transparent home"
        id="home"
      >
        <div className="">
          <div className="cedricmedia-header-inner">
            <div className="nav-container fixed-top">
              {/* <h1 className="cedricmedia-logo text-uppercase pull-left">Roller</h1> */}
              <div onClick={() => this.logoClickHandler()}>
                <CedricmediaLogo></CedricmediaLogo>
              </div>
              {/* <img
								onClick={() => this.logoClickHandler()}
								className="cm-logo"
								src={require("../assets/img/cm-2015-w-c.svg")}
							></img> */}

              <nav className="d-none d-md-block cedricmedia-nav text-uppercase">
                <ul className="menu-holder">{this.navLinkData()}</ul>
              </nav>
              <div onClick={() => this.hamClickHandler()}>
                <Hamburger></Hamburger>
              </div>
            </div>
          </div>
          {/* <img
						src={require("../assets/img/tm-border-black-bottom.png")}
						alt="Border"
						className="tm-border header-black-angle-border flipped-image"
					></img> */}
        </div>
        <MobileNav
          hamClickHandler={this.hamClickHandler}
          isOpen={this.state.isMenuOpen}
        ></MobileNav>
        <div className="container">
          <div className="main-hero-content">
            <div className="row">
              <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <img
                  src={require("../assets/img/cedricmedia-banner.png")}
                  alt="Banner"
                  className="img-responsive center-block"
                ></img>
              </div>
              <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div className="tm-home-right">
                  <div className="main-hero-h1-wrap">
                    <h1 className="tm-home-title text-uppercase">Welcome</h1>
                  </div>
                  <div className="main-hero-h2-wrap">
                    <h2 className="tm-home-subtitle tm-orange-text text-uppercase">
                      We're problem-solvers
                    </h2>
                  </div>
                  <div className="tm-home-description">
                    <p>
                      Tell us about your vision and let's see what we can build
                      together. we are driven by our primary core values:
                      responsive customer service, experience, and passion. Our
                      deep understanding and love for code helps us create
                      unique solutions to streamline your website.
                    </p>
                  </div>
                  <a
                    href="#"
                    className="tm-home-more tm-dark-text tm-orange-bg"
                  >
                    View More
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <img
          src={require("../assets/img/tm-border-white-top.png")}
          alt="Border"
          className="tm-border flipped-image-h-v triangle-nudge"
        ></img>
      </section>
    );
  }
}

export default Home;
