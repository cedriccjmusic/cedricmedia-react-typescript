import React, { Component } from "react";
import "./Testimonials.scss";
import testimonials from "../assets/json/testimonials.json";
import * as models from "../models/cm-models";
import * as siteData from "../firestore/siteDataService";
import { firestore } from 'firebase/app';

interface ITestimonialProps {}

interface ITestimonialState {
	active: number;
	shouldRenderData: boolean;
}

class Testimonials extends Component<ITestimonialProps, ITestimonialState> {
	private testimonials: models.ITestimonial[] = [];
	private imgBaseUrl: string = "../assets/img/";

	state: ITestimonialState;

	constructor(props: ITestimonialProps) {
		super(props);
		
		this.state = {
			active: 2,
			shouldRenderData: false
		};
	}
	componentDidMount() {
		this.getAllData();
	}

	getAllData = () => {
		siteData
			.getSiteData()
			.then(doc => {
				if (doc.exists) {
					let data:
						| firestore.DocumentData
						| undefined = doc.data() as models.ISiteConfiguration;
					// this.setState({ data: data });
					this.testimonials = data.testimonials;

					this.setState({
						shouldRenderData: true
					});
					return data;
				} else {
					// doc.data() will be undefined in this case
					// this.setState({ data: null });
					let data = null;
					console.debug("No such document!");
					return data;
				}
			})
			.catch(error => {
				// this.setState({ data: null });
				let data = null;
				console.debug("Error getting document:", error);
				return data;
			});
	};

	handleImgClick = (key: number) => {
		this.setState({
			active: key
		});
	};

	testimonialData = () =>
		this.testimonials.map((testimonial, key) => (
			<div className="img-container" key={key}>
				<div
					className={
						"tm-img-circle-border absolute-center " +
						(this.state.active === key ? "active" : "no-show")
					}
					onClick={() => this.handleImgClick(key)}
					data-comment={testimonial.comment}
					data-author={testimonial.author}
					id={testimonial.id}
				>
					<img
						src={require("../assets/img/" + testimonial.img)}
						alt={testimonial.altText}
						className="img-circle tm-img-testimonial"
					></img>
				</div>
			</div>
		));
	commentHandler = () => {
		return this.state.shouldRenderData === true ? this.testimonials[this.state.active].comment : null;
	}

	authorHandler = () => {
		return this.state.shouldRenderData === true ? this.testimonials[this.state.active].author : null;
	}

	render() {
		return (
			<section className="tm-section testimonials" id="testimonials">
				<img
					src={require("../assets/img/tm-border-black-top.png")}
					alt="Border"
					className="tm-border flipped-image-h"
				></img>
				<div className="container">
					<h1 className="text-uppercase text-center">Testimonials</h1>
					<div className="text-center margin-top-50 tm-testimonial-images-container">
						{this.testimonialData()}
					</div>
					<p className="text-center margin-top-50 tm-comment">
						<em>
						{this.commentHandler()}
						</em>
					</p>
					<p className="text-uppercase text-center tm-author tm-orange-text">
						{this.authorHandler()}
					</p>
					<hr className="tm-author-name-underline"></hr>
				</div>
			</section>
		);
	}
}

export default Testimonials;
