import React, { Component } from "react";
import firebase from 'firebase/app';
import './Firestore';
import navLinks from "../assets/json/navLinks.json";
import portfolio from "../assets/json/portfolio.json";
import testimonials from "../assets/json/testimonials.json";
import services from "../assets/json/services.json";
import * as models from "../models/cm-models";



export const getSiteData = () => {
    let siteConfig: models.ISiteConfiguration;
    siteConfig = {
        navLinks,
        portfolio,
        testimonials,
        services
    };
    let db = firebase.firestore();

    const docRef = db.collection("frontEndData").doc("cedricmedia-react");

    docRef.set(siteConfig);

    return docRef.get();
        
}
