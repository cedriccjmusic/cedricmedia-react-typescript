import firebase from 'firebase/app';
import firestore from "firebase/firestore";

// const settings = { timestampsInSnapshots: true };

const config = {
	apiKey: "AIzaSyCe9-aIB-GVYOSN0VvH9pV_BwPtgG8BcVc",
	authDomain: "cedricmedia-react.firebaseapp.com",
	databaseURL: "https://cedricmedia-react.firebaseio.com",
	projectId: "cedricmedia-react",
	storageBucket: "cedricmedia-react.appspot.com",
	messagingSenderId: "986038827719",
	appId: "1:986038827719:web:37298218024a228b961ce2",
	measurementId: "G-KZP678HEJZ"
};
firebase.initializeApp(config);

// firebase.firestore().settings(settings);
firebase.firestore();

export default firebase;
