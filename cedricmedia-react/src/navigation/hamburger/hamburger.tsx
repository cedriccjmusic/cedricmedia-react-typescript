import React, { Component } from "react";
import "./hamburger.scss";

interface IHamburgerProps {
}
class Hamburger extends Component<IHamburgerProps> {
	constructor(props: IHamburgerProps) {
		super(props);
	}

	render() {
		return (
			<div className="text-right d-md-none">
				<a href="#" id="mobile_menu">
					<span className="fa fa-bars"></span>
				</a>
			</div>
		);
	}
}

export default Hamburger;
