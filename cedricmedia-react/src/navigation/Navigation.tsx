import React, { Component } from "react";
import "./Navigation.scss";
import DesktopNav from './desktop-nav/DesktopNav';
import MobileNav from './mobile-nav/MobileNav';

interface IAppProps {}

class Navigation extends Component<IAppProps, any> {
	constructor(props: IAppProps) {
		super(props);

		this.state = {
			isGlazed: false,
			isFrosted: true
		};
	}

	render() {
		return (
			<div className="nav-wrap">
                <DesktopNav></DesktopNav>
            </div>
		);
	}
}

export default Navigation;
