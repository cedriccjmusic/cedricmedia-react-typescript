import React, { Component } from "react";
import "./MobileNav.scss";
import PropTypes from "prop-types";
import navLinks from "../../assets/json/navLinks.json";
import * as models from "../../models/cm-models";
import $ from "jquery";
import * as siteData from "../../firestore/siteDataService";
import { firestore } from 'firebase/app';

interface IMobileNavProps {
	isOpen: boolean;
	hamClickHandler: () => void;
	navBarHeight?: number;
	pageSection?: string;
}

interface IMobileNavState {
	shouldCloseMenu: boolean;
	activeNavLink: number;
	shouldRenderData?: boolean;
}

class MobileNav extends React.Component<IMobileNavProps, IMobileNavState> {
	private navLinks: models.INavLink[] = [];
	constructor(props: IMobileNavProps) {
		super(props);
		this.state = {
			shouldCloseMenu: true,
			activeNavLink: 0,
			shouldRenderData: false
		};
	}

	componentDidMount() {
		this.getAllData();
	}

	getAllData = () => {
		siteData
			.getSiteData()
			.then(doc => {
				if (doc.exists) {
					let data:
						| firestore.DocumentData
						| undefined = doc.data() as models.ISiteConfiguration;
					// this.setState({ data: data });
					this.navLinks = data.navLinks;

					this.setState({
						shouldRenderData: true
					});
				} else {
					// doc.data() will be undefined in this case
					// this.setState({ data: null });
					let data = null;
				}
			})
			.catch(error => {
				// this.setState({ data: null });
				let data = null;
				return data;
			});
	};

	menuClick = () => {
		this.setState({
			shouldCloseMenu: true
		});
		this.shouldToggleMenu();
		console.debug("menu: ", this.state.shouldCloseMenu);
	};
	shouldToggleMenu = () => {
		return this.props.isOpen;
	};

	handleNavLinkClick = (key: number) => {
		this.setState({
			activeNavLink: key
		});

		let matchingSection = this.navLinks[key].href;
		let navbarHeight = $(".nav-container").height();
		$("html,body").animate(
			{
				scrollTop: $("" + matchingSection).offset()!.top - navbarHeight!
			},
			"slow"
		);
	};

	navLinkData = () =>
		this.navLinks.map((navLink, key) => (
			<li key={key}>
				<a
					href={navLink.href}
					className={
						"nav-btn " +
						navLink.class +
						(this.state.activeNavLink === key ? " active" : "")
					}
					onClick={() => this.handleNavLinkClick(key)}
				>
					<i className={"fa " + navLink.icon}></i>
					{navLink.name}
				</a>
			</li>
		));
	render() {
		return (
			<div
				id="responsive-menu"
				className={this.shouldToggleMenu() === true ? "open" : "closed"}
			>
				<ul className="menu-holder" onClick={this.props.hamClickHandler}>
					{this.navLinkData()}
				</ul>
			</div>
		);
	}
}

export default MobileNav;
