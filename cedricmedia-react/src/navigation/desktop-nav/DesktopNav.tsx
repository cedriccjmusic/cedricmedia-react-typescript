import React, { Component } from "react";
import "./DesktopNav.scss";
import Hamburger from "../hamburger/hamburger";
import MobileNav from "../mobile-nav/MobileNav";
import $ from "jquery";
import navLinks from "../../assets/json/navLinks.json";
import * as models from "../../models/cm-models";
import * as siteData from "../../firestore/siteDataService";
import { firestore } from "firebase/app";

import "../../assets/jQuery/customScrolling.js";
import * as customScrolling from "../../assets/jQuery/customScrolling.js";

interface IDesktopNavProps {}
interface IDesktopNavState {
	isMenuOpen: boolean;
	activeNavLink: number;
	shouldRenderData: boolean;
}

class DesktopNav extends Component<IDesktopNavProps, IDesktopNavState> {
	private navLinks: models.INavLink[] = [];

	// state: IDesktopNavState;

	constructor(props: IDesktopNavProps) {
		super(props);
	}

	

	render() {
		return (
			<section>
				
			</section>
		);
	}
}

export default DesktopNav;
