import React, { Component } from "react";
import "./Contact.scss";
import { string } from "prop-types";
import * as models from "../models/cm-models";
import firebase from "firebase/app";
import * as Yup from "yup";
import * as contactTypes from "./contact-models";

interface IContactProps {}
interface IContactState
	extends contactTypes.iName,
		contactTypes.iEmail,
		contactTypes.iSubject,
		contactTypes.iMessage {
	message: string;
	messageCharLength: number;
	contactFormIsValid: boolean;
}

class Contact extends Component<IContactProps, IContactState> {
	constructor(props: IContactProps) {
		super(props);

		this.state = {
			...this.state,
			nameErrors: { nameNotProvided: false },
			emailErrors: { emailNotProvided: false, validEmail: true },
			subjectErrors: { subjectNotProvided: false },
			messageErrors: { messageNotProvided: false },
			name: "",
			email: "",
			subject: "",
			message: "",
			messageCharLength: 0,
			contactFormIsValid: false
		};
	}

	nameValidator = (e: React.ChangeEvent<any>) => {
		e.persist();
		let name = e.target.value;
		let notProvided = false;
		let ready = false;

		if (name === "") {
			notProvided = true;
			ready = false;
		} else {
			notProvided = false;
			ready = true;
		}

		this.setState({
			name: name,
			nameErrors: {
				nameNotProvided: notProvided
			},
			nameReady: ready
		});
		this.contactFormValidState(e);
	};

	emailValidator = (e: React.ChangeEvent<any>) => {
		e.persist();

		let email = e.target.value;
		let notProvided = false;
		let valid = false;
		let ready = false;

		if (email === "") {
			notProvided = true;
			ready = false;
		} else if (/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email) === false) {
			valid = false;
			notProvided = true;
			ready = false;
		} else {
			valid = true;
			notProvided = false;
			ready = true;
		}

		this.setState({
			email: email,
			emailErrors: {
				emailNotProvided: notProvided,
				validEmail: valid
			},
			emailReady: ready
		});

		this.contactFormValidState(e);
	};

	subjectValidator = (e: React.ChangeEvent<any>) => {
		e.persist();

		let subject = e.target.value;
		let notProvided = false;
		let ready = false;

		if (subject === "") {
			notProvided = true;
			ready = false;
		} else {
			notProvided = false;
			ready = true;
		}

		this.setState({
			subject: subject,
			subjectErrors: {
				subjectNotProvided: notProvided
			},
			subjectReady: ready
		});

		this.contactFormValidState(e);
	};

	getCharacterCount = (e: React.ChangeEvent<any>) => {
		e.persist();
		const message: string = e.target.value;
		this.setState({
			message: message,
			messageCharLength: message.length
		});
	};

	messageValidator = (e: React.ChangeEvent<any>) => {
		e.persist();
		this.getCharacterCount(e);

		let message = e.target.value;
		let notProvided = false;
		let ready = false;

		if (message === "") {
			notProvided = true;
			ready = false;
		} else {
			notProvided = false;
			ready = true;
		}

		this.setState({
			message: message,
			messageErrors: {
				messageNotProvided: notProvided
			},
			messageReady: ready
		});

		this.contactFormValidState(e);
	};

	contactFormValidState = (e: React.ChangeEvent<any>) => {
		e.preventDefault();
		if (
			this.state.nameReady === true &&
			this.state.emailReady === true &&
			this.state.subjectReady === true &&
			this.state.messageReady === true
		) {
			this.setState(prevState => ({
				contactFormIsValid: true
			}));
		} else {
			this.setState(prevState => ({
				contactFormIsValid: false
			}));
		}
	};

	contactFormSubmissionHandler = (e: React.ChangeEvent<any>) => {
		e.preventDefault();
		let formFields: {} = {
			name: this.state.name,
			email: this.state.email,
			subject: this.state.subject,
			message: this.state.message
		};
		const db = firebase.firestore();
		const userRef = db.collection("cedricmedia-contact-form").add(formFields);
	};

	render() {
		return (
			<section className="tm-section tm-orange-bg contact" id="contact">
				<img
					src={require("../assets/img/tm-border-white-top.png")}
					alt="Border"
					className="tm-border"
				></img>
				<div className="container">
					<div className="contact-form-wrap">
						<h1 className="text-uppercase text-center">
							We’d Love to Hear from You
						</h1>
						<p className="text-center margin-top-50">
							We really do answer the phone. Give us a call or stop by our
							office downtown and let's talk about building a project that
							solves your business needs.
						</p>
						<div className="row margin-top-50">
							<div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<form
									className="contact-form"
									onSubmit={this.contactFormSubmissionHandler}
									noValidate
								>
									<div className="row">
										<div className="col-md-6 offset-md-3">
											<div className="form-group">
												<label htmlFor="subject">Name</label>
												<input
													type="text"
													name="subject"
													onBlur={this.nameValidator}
													onChange={this.nameValidator}
													onKeyUp={this.nameValidator}
													required
												/>
												<div className="error-msg">
													<div>
														{this.state.nameErrors.nameNotProvided === true && (
															<p>Please provide you name.</p>
														)}
													</div>
												</div>
											</div>

											<div className="form-group">
												<label htmlFor="email">Email</label>
												<input
													type="email"
													name="email"
													onBlur={this.emailValidator}
													onChange={this.emailValidator}
													onKeyUp={this.emailValidator}
													required
												/>
												<div className="error-msg">
													<div>
														{this.state.emailErrors.validEmail === false && (
															<p>
																Invalid email. Correct format: john@google.com
															</p>
														)}
														{this.state.emailErrors.emailNotProvided ===
															true && <p>Please provide your email.</p>}
													</div>
												</div>
											</div>

											<div className="form-group">
												<label htmlFor="subject">Subject</label>
												<input
													type="text"
													name="subject"
													onBlur={this.subjectValidator}
													onChange={this.subjectValidator}
													onKeyUp={this.subjectValidator}
													required
												/>
												<div className="error-msg">
													<div>
														{this.state.subjectErrors.subjectNotProvided ===
															true && <p>Please provide a msg subject.</p>}
													</div>
												</div>
											</div>

											<div className="form-group">
												<label htmlFor="message">Message</label>
												<textarea
													maxLength={100}
													name="message"
													onBlur={(evt: React.ChangeEvent<any>) =>
														this.messageValidator(evt)
													}
													onChange={(evt: React.ChangeEvent<any>) =>
														this.messageValidator(evt)
													}
													onKeyUp={(evt: React.ChangeEvent<any>) =>
														this.messageValidator(evt)
													}
													required
												/>
												<span
													className={`character-count ${this.state
														.messageCharLength >= 101 && "red"} `}
												>
													{this.state.messageCharLength}/100
												</span>
												<div className="error-msg">
													<div>
														{this.state.messageErrors.messageNotProvided ===
															true && <p>Please provide a message.</p>}
														{this.state.messageCharLength >= 101 && (
															<p>
																Character limit of{" "}
																{this.state.messageCharLength} reached.
															</p>
														)}
													</div>
												</div>
											</div>
											<button
												disabled={this.state.contactFormIsValid === false}
												className="btn btn-primary contact-submit-btn"
												type="submit"
											>
												Submit
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<img
					src={require("../assets/img/tm-border-black-bottom.png")}
					alt="Border"
					className="tm-border flipped-image-h"
				></img>
			</section>
		);
	}
}

export default Contact;
