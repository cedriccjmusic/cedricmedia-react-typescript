
export interface iName {
	name: string;
	nameErrors: {
		nameNotProvided: boolean
	},
	nameReady: boolean;
}
export interface iEmail {
	email: string;
	emailErrors: {
		emailNotProvided: boolean;
		validEmail: boolean
	},
	emailReady: boolean;
}
export interface iSubject {
	subject: string;
	subjectErrors: {
		subjectNotProvided: boolean
	},
	subjectReady: boolean;
}
export interface iMessage {
	message: string;
	messageErrors: {
		messageNotProvided: boolean
	},
	messageReady: boolean;
}