import React, { Component } from "react";
import "./Portfolio.scss";
import portfolio from "../assets/json/portfolio.json";
import * as models from "../models/cm-models";
import _ from "lodash";
import * as siteData from "../firestore/siteDataService";
import { firestore } from 'firebase/app';

interface IPortfolioProps {}
interface IPortfolioState {
	shouldViewMore: boolean;
	shouldRenderData: boolean;
}
class Portfolio extends Component<IPortfolioProps, IPortfolioState> {
	private portfolio: models.IPortfolio[] = [];

	constructor(props: IPortfolioProps) {
		super(props);

		this.state = {
			shouldViewMore: true,
			shouldRenderData: false
		};
	}

	componentDidMount() {
		this.getAllData();
	}

	getAllData = () => {
		siteData
			.getSiteData()
			.then(doc => {
				if (doc.exists) {
					let data:
						| firestore.DocumentData
						| undefined = doc.data() as models.ISiteConfiguration;
					// this.setState({ data: data });
					this.portfolio = data.portfolio;
					
					this.portfolio = _.dropRight(this.portfolio, this.portfolio.length / 2);

					console.debug("all set?: ", this.portfolio);
					this.setState({
						shouldRenderData: true
					});
					return data;
				} else {
					// doc.data() will be undefined in this case
					// this.setState({ data: null });
					let data = null;
					console.debug("No such document!");
					return data;
				}
			})
			.catch(error => {
				// this.setState({ data: null });
				let data = null;
				console.debug("Error getting document:", error);
				return data;
			});
	};

	viewMoreClickHandler = () => {
		this.setState(previousState => ({
			shouldViewMore: !previousState.shouldViewMore
		}));
		if (this.state.shouldViewMore === false) {
			this.portfolio = _.dropRight(portfolio, portfolio.length / 2);
		} else {
			this.portfolio = portfolio;
		}
	};

	portfolioData = () =>
		this.portfolio.map((portfolio, key) => (
			<div className={"tm-image-container " + portfolio.hideClass} key={key}>
				<div className="tm-portfolio-image-overlay text-center">
					<div className="absolute-center">
						<a href={portfolio.link} className="fa fa-search"></a>
						<h2 className="text-uppercase tm-white-text">
							{portfolio.heading}
						</h2>
						<p className="text-uppercase">{portfolio.description}</p>
					</div>
				</div>
				<img
					src={require("../assets/img/" + portfolio.imgUrl)}
					alt={portfolio.altText}
				></img>
			</div>
		));

	render() {
		return (
			<section className="tm-dark-bg tm-section portfolio" id="portfolio">
				<div className="container">
					<div className="row">
						<div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
							<h1 className="tm-light-gray-text text-center text-uppercase">
								Portfolio
							</h1>
							<div className="tm-portfolio-images-container margin-top-50">
								{this.portfolioData()}
							</div>
							<a
								onClick={() => this.viewMoreClickHandler()}
								href="javascript:void(0)"
								className="tm-view-more tm-light-gray-text margin-top-50 text-center text-uppercase"
							>
								{this.state.shouldViewMore ? "View More" : "View Less"}
							</a>
						</div>
					</div>
				</div>
			</section>
		);
	}
}

export default Portfolio;
