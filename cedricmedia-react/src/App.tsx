import React from 'react';
import './assets/css/style.scss';
import Navigation from './navigation/Navigation';
import Home from './home/Home';
import About from './about/About';
import Services from './services/Services';
import Portfolio from './portfolio/Portfolio';
import Testimonials from './testimonials/Testimonials';
import Contact from './contact/Contact';


const App: React.FC = () => {
  return (
    <div className="App">
      <Home></Home>
      <About></About>
      <Services></Services>
      <Portfolio></Portfolio>
      <Testimonials></Testimonials>
      <Contact></Contact>
    </div>
  );
}

export default App;
