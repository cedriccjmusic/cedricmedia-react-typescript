import React, { Component } from "react";
import "./About.scss";

interface IAppProps {}

class About extends Component<IAppProps, any> {
  constructor(props: IAppProps) {
    super(props);

    this.state = {
      isGlazed: false,
      isFrosted: true
    };
  }

  render() {
    return (
      <section className="cedricmedia-about tm-section about" id="about">
        <div className="container">
          <div className="row">
            <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <div className="about-img-wrapper">
                <div id="member1" className="member">
                  <div className="canvas-wrap canvas-lines">
                    <div className="canvas-overlay">
                      <div className="tm-member-info absolute-center">
                        <div className="tm-member-wrap">
                          <p className="member-name text-center text-uppercase">
                            Shane Bader
                          </p>
                          <p className="member-title text-center text-uppercase">
                            Web designer
                          </p>
                          <div className="tm-member-social-info text-center">
                            <a href="#" className="fa fa-twitter"></a>
                            <a href="#" className="fa fa-facebook"></a>
                            <a href="#" className="fa fa-linkedin"></a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <div className="about-img-wrapper">
                <div id="member2" className="member">
                  <div className="canvas-wrap canvas-lines">
                    <div className="canvas-overlay">
                      <div className="tm-member-info absolute-center">
                        <div className="tm-member-wrap">
                          <p className="member-name text-center text-uppercase">
                            Chrissy Larson
                          </p>
                          <p className="member-title text-center text-uppercase">
                            Graphic designer
                          </p>
                          <div className="tm-member-social-info text-center">
                            <a href="#" className="fa fa-twitter"></a>
                            <a href="#" className="fa fa-facebook"></a>
                            <a href="#" className="fa fa-linkedin"></a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <div className="about-img-wrapper">
                <div id="member3" className="member">
                  <div className="canvas-wrap">
                    <div className="canvas-overlay">
                      <div className="tm-member-info absolute-center">
                        <div className="tm-member-wrap">
                          <p className="member-name text-center text-uppercase">
                            Kyle Teeter
                          </p>
                          <p className="member-title text-center text-uppercase">
                            Web developer
                          </p>
                          <div className="tm-member-social-info text-center">
                            <a href="#" className="fa fa-twitter"></a>
                            <a href="#" className="fa fa-facebook"></a>
                            <a href="#" className="fa fa-linkedin"></a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <div className="tm-about-right">
                <h1 className="tm-about-title text-uppercase">Meet</h1>
                <h2 className="tm-about-subtitle tm-orange-text text-uppercase">
                  Our Team
                </h2>
                <h3 className="tm-about-subtitle-2 text-uppercase">
                  Consectetur Deostrla
                </h3>
                <div className="tm-about-description">
                  <ul>
                    <li>Pellentesque aliquam nisi</li>
                    <li>Rondimentum nulla sagittis ura</li>
                    <li>Gconsectetur imperdiet</li>
                    <li>Dconsect condimentum etur</li>
                    <li>Praesent consectur mulla</li>
                    <li>Sdui accumsan pharetra</li>
                    <li>Dconsect condimentum etur</li>
                    <li>Gconsectetur imperdiet</li>
                  </ul>
                </div>
                <h3 className="tm-about-subtitle-2">Lorem Ipsums</h3>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default About;
